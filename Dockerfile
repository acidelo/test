FROM python:3.7.3 as builder

RUN mkdir /app
WORKDIR /app
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
COPY src .
RUN pyinstaller ./main.spec

FROM python:3.7.3
COPY --from=builder /app/dist/main /app/
CMD [ "/app/main" ]

