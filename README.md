# Code Protection Assignment #
## Overview 
We are shipping a docker container with python code in it to a customerWe do not want the code to be visible to the customer - for protecting our IP. We would like to block our customer from understanding how is our code working

In order to solve it there are a few options: 
1. Allow running the docker but do not allow prompt access to the docker (docker exec -it dockername /bin/sh)
2. find another way for blocking the source file content - but still being able to run the code 
3. use code obfuscation solutions (compilation, ) 
4. Use a combination of one or more solutions 


### Running the current code ###
in a python 3.7 environment :

```
pip install -r requirements.txt 
uvicorn main:app --reload
```

## Your tasks ##
1. Clone this repository and branch it to a public repository or a private repository and give us access
2. Implement your solution for code protection - you can use more than one solution
3. Add the implementation to the Dockerfile - so the process is done while building the Docker container
4. Add to this file explains how to use your solution. 


## Solution explanation ##

The first option *Allow running the docker but do not allow prompt access to the docker (docker exec -it dockername /bin/sh* is not possible because even if you remove all the binaries (such as `sh`) and leave the necessary files to run the app you can still execute `docker cp dockername /source.py .` which is gonna copy the selected file. For this reason this option is not suitable.

There's a way to create an executable out of python applications with a package called `pyinstaller`. In order to use it we need to find the hidden imports of the packages. Considering the small script this is not a problem because we need to declare the hidden imports of uvicorn and the main script itself. In the case of larger applications, for example, with django, there are some cool guidelines with information of how to properly configure the spec file to create the binary out of the django app.

Anyways. Having the binary is not enough to protect the code within a Docker image because the code is still available in the filesystem, so we can use a multi-step Dockerfile. Having a multisetp Dockerfile allows us to squash the previous images, so the resultant layers only include the second step of the build, which basically copies the binary from the first build step. This will also help to have a lighter image.

The resultant docker image only contains the binary and the python 3 utilities.

To build the image you can run

```
docker build -t keneti/main_app .
```

In order to share the image you can either publish it to a registry or save it as tar.

```
# push to docker registry
docker push keneti/main_app

# save as tar
docker save keneti/main_app > main_app.tar
```

From another machine you can pull it or load it

```
# pull the image
docker pull keneti/main_app

# load it
docker load --input main_app.tar
```

Then you can run the application with the following command:

```
docker run -d -p 5000:5000 --name main_app keneti/main_app
```

You can test it with:

```
curl localhost:5000
```

Small note: I added `main.spec` file and changed the `.gitignore` file to allow doing that.