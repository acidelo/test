# -*- mode: python -*-

block_cipher = None


a = Analysis(['main.py'],
             pathex=['/app/src'],
             binaries=[],
             datas=[],
             hiddenimports=['main','uvicorn.logging','uvicorn.loops','uvicorn.loops.auto','uvicorn.loops.asyncio','uvicorn.loops.uvloop','uvicorn.protocols','uvicorn.protocols.http','uvicorn.protocols.http.auto','uvicorn.protocols.http.h11_impl','uvicorn.protocols.http.httptools_impl','uvicorn.protocols.websockets','uvicorn.protocols.websockets.auto','uvicorn.protocols.websockets.websockets_impl','uvicorn.protocols.websockets.wsproto_impl','uvicorn.lifespan','uvicorn.lifespan.on','uvicorn.lifespan.off'],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          [],
          name='main',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          runtime_tmpdir=None,
          console=True )
